# theHam project

### Landing page template with connection of dynamic elements  

theHam page is available on ```GitLab Pages``` by [link](https://stanislav.kosinskiy.gitlab.io/the-ham-project/)  
The page is based on the psd templates that are located [here](https://gitlab.com/stanislav.kosinskiy/the-ham-project/tree/master/psd)

##### The project uses the next stack of technologies:  
  
- HTML5  
- CSS3  
- JS(ES6)  
- jQuery  
  
##### Used jQuery plugins:  
- Masonry  
- JCarousel  



