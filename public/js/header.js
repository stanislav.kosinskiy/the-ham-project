"use strict";

$(() => {

    const NAVBAR_MENU_LINK_SELECTOR = ".navbar-menu-link";
    const NAVBAR_HOME_MENU_LINK_SELECTOR = NAVBAR_MENU_LINK_SELECTOR + "[href='#header']";
    const SCROLL_TOP_LAYER_SELECTOR = ".scroll-top-layer";
    let defaultScrollPos = 0;

    /**
     * Function for smooth scrolling to anchor.
     * @returns {void}
     * */

    function scrollToAnchor () {
        let elementClick = $(this).attr("href");
        let destination = $(elementClick).offset().top;
        let duration = destination;
        if (duration < 500) {
            duration = 500;
        } else if (duration > 1500) {
            duration = 1500;
        }
        $("html").animate({
            scrollTop: destination
        }, duration);
    }

    /**
     * Function to show search field.
     * @returns {void}
     * */

    function showSearchField() {
        $(this).hide().siblings().show(300).children().focus().focusout(function () {
            $(this).parent().hide().siblings().show();
        });
    }

    /**
     * Function to show scroll top button depending on window position.
     * @returns {void}
     * */
    
    function showScrollTopButton() {
        if ($(window).scrollTop() >= $(window).height()*2) {
            $(SCROLL_TOP_LAYER_SELECTOR + ".hidden").fadeIn(500);
        } else {
            $(SCROLL_TOP_LAYER_SELECTOR).fadeOut(500);
        }
    }

    /**
     * Function to scroll to the top anchor.
     * @returns {void}
     * */

    function scrollToTop() {
        $(NAVBAR_HOME_MENU_LINK_SELECTOR).trigger("click");
    }

    /**
     * Function to make home menu link active on scrolling to the top.
     * @returns {void}
     * */

    function checkNavbarLinkActive() {
        let currentScrollPos = $(window).scrollTop();
        if (currentScrollPos < 120 && currentScrollPos < defaultScrollPos) {
            $(NAVBAR_HOME_MENU_LINK_SELECTOR).addClass("active").parent().siblings().children().removeClass("active");
        }
        defaultScrollPos = currentScrollPos;
    }

    $(NAVBAR_MENU_LINK_SELECTOR).on("click", makeMenuLinkActive);
    $(".button-with-layer[href='#purchase']").on("click", scrollToAnchor);
    $(NAVBAR_MENU_LINK_SELECTOR).on("click", scrollToAnchor);
    $(".navbar-menu-search .header-search-icon").on("click", showSearchField);
    $(window).on("scroll", showScrollTopButton);
    $(window).on("scroll", checkNavbarLinkActive);
    $(SCROLL_TOP_LAYER_SELECTOR).on("click", scrollToTop);
});


