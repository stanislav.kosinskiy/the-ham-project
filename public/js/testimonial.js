$(() => {
    // This is the connector function.
    // It connects one item from the navigation carousel to one item from the
    // stage carousel.
    // The default behaviour is, to connect items with the same index from both
    // carousels. This might _not_ work with circular carousels!
    let connector = function(itemNavigation, carouselStage) {
        return carouselStage.jcarousel('items').eq(itemNavigation.index());
    };

    $(function() {
        // Setup the carousels. Adjust the options for both carousels here.
        let carouselStage      = $('.testimonial-carousel-stage').jcarousel({
            wrap:"circular"
        });
        let carouselNavigation = $('.testimonial-carousel-navigation').jcarousel();

        // We loop through the items of the navigation carousel and set it up
        // as a control for an item from the stage carousel.
        carouselNavigation.jcarousel('items').each(function() {
            let item = $(this);

            // This is where we actually connect to items.
            let target = connector(item, carouselStage);

            item
                .on('jcarouselcontrol:active', function() {
                    carouselNavigation.jcarousel('scrollIntoView', this);
                    item.addClass('active');
                })
                .on('jcarouselcontrol:inactive', function() {
                    item.removeClass('active');
                })
                .jcarouselControl({
                    target: target,
                    carousel: carouselStage
                });
        });
        // Setup controls for the stage carousel
        $('.prev-stage')
            .jcarouselControl({
                target: '-=1'
            });

        $('.next-stage')
            .jcarouselControl({
                target: '+=1'
            });
    });
});
