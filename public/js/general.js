"use strict";

/**
 * Universal function to add class "active" to menu link and remove it for all another links.
 * @param event - event initializing function.
 * @returns {void}
 * */

function makeMenuLinkActive (event) {
    event.preventDefault();
    $(this).addClass("active").parent().siblings().children().removeClass("active");
}

/**
 * Universal function to remove class of set items quantity.
 * @param items - jQuery object containing items to remove class.
 * @param quantity - count of items to remove class.
 * @param className - class name to remove.
 * @returns {void}
 * */

function removeItemsClass(items, quantity, className) {
    items.each((index, element) => index < quantity ? $(element).removeClass(className) : null);
}

/**
 * Universal function to filter items by attribute value of another element.
 * @param itemsSelector - string selector of items to be filtered.
 * @param filterElementSelector - string selector of element containing filter criteria as attribute.
 * @param attribute - string attribute containing filter criteria as value.
 * @returns {jQuery} - jQuery object consisting of filtered items.
 * */

function getFilteredByAttributeItems(itemsSelector, filterElementSelector, attribute) {
    let criteria = $(filterElementSelector).attr(attribute);
    return criteria === "all" ? $(itemsSelector) : $(itemsSelector+"[" + attribute + "=" + criteria + "]");
}

/**
 * Universal function to check is it necessary to show or hide "load more" button depending on count of "hidden items" left.
 * Should be called after pressing "load more" button and/or switching filter category.
 * @param hiddenItems - jQuery object consisting of hidden items.
 * @param buttonSelector - string selector of "load more" button.
 * @param buttonPushCounter - number of current "load more" button pressing.
 * @param maxButtonPushCounter - maximum possible number of current "load more" button pressing.
 * @returns {void}
 * */

function checkButtonDisplay(hiddenItems, buttonSelector, buttonPushCounter, maxButtonPushCounter) {
    let loadMoreButton = $(buttonSelector);
    hiddenItems.length === 0 || buttonPushCounter === maxButtonPushCounter ? loadMoreButton.hide() : loadMoreButton.show();
}

/**
 * Universal function to use masonry plugin
 * @param gridSelector - string selector of parent grid element.
 * @param gridItemSelector - string selector of grid element.
 * @returns {void}
 * */

function useMasonry(gridSelector, gridItemSelector) {
    let $grid = $(gridSelector).masonry({
        itemSelector: gridItemSelector,
        columnWidth: 10,
        gutter: 11,
        fitWidth: true,
    });
    $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
    });
}

