"use strict";

$(() => {

    /**
     * Function to switch services tabs.
     * @returns {void}
     * */

    function switchServicesTab() {
        $(".services-description").children().addClass("hidden").parent().eq($(this).index()).children().removeClass("hidden");
    }

    $(".services-menu-link").on("click", makeMenuLinkActive);
    $(".services-menu-item").on("click", switchServicesTab);
});