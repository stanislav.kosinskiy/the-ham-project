"use strict";

$(() => {

    const MODAL_LAYER_SELECTOR = ".modal-layer.hidden";

    /**
     * Function to show modal window and hide on clicking outside the window or "order" button.
     * @param event - event initializing function.
     * @returns {void}
     * */

    function showModalWindow (event) {
       event.preventDefault();
       $(MODAL_LAYER_SELECTOR).fadeIn(500).on("click", (event) => {
           if ($(event.target).attr("class") === "modal-layer hidden" || $(event.target).attr("class") === "modal-form-button modal-field") {
               $(MODAL_LAYER_SELECTOR).fadeOut(500);
           }
       });
    }
    $(".modal-trigger").on("click", showModalWindow)
});